# Sample Grails Web Service

This is an example implementation of the web service described in out [Integration Guide](https://docs.google.com/document/d/1OlkZ3VEQGOXr1mrjHySK4wJUPznu3O6L9YsJBAkBByU/edit#). It is build in Groovy and Grails and runs on the Java Virtual Machine.

## Running the Web Service

```
brew install grails
cd sample-grails-web-service
grails run-app
```

## Calling the Web Service

Install [Postman](https://www.getpostman.com/) and import postman_collection.json. You will see sample requests for the two operations: GetChangeLog and GetMember.