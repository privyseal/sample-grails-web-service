package privyseal.api

class MemberController {

    static responseFormats = ['json']

    def ACCESS_KEY = "1234";

    def getMember() {

        def accessKey = request.JSON?.AccessKey
        def memberId = request.JSON?.MemberID

        if (!ACCESS_KEY.equals(accessKey)) {
            response.status = 401
            respond (message:"Invalid AccessKey")
            return
        }

        def member = Member.findByMemberNumber(memberId)

        if (!member) {
            response.status = 404
            respond (message:"Member not found")
            return
        }

        respond member;

    }

}
