package privyseal.api

class ChangeLogController {

    static responseFormats = ['json']

    def ACCESS_KEY = "1234";

    def list() {        

        def accessKey = request.JSON?.AccessKey
        def fromChangeLogId = 0
        if (request.JSON?.FromChangeLogID) {
            fromChangeLogId = request.JSON?.FromChangeLogID
        }
        def max = 1000
        if (request.JSON?.Limit) {
            max = request.JSON?.Limit
        }

        if (!ACCESS_KEY.equals(accessKey)) {
            response.status = 401
            respond (message:"Invalid AccessKey")
            return
        }

        respond ChangeLog.findAllByIdGreaterThanEquals(fromChangeLogId, [max: max])
    }

}
