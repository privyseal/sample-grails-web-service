package privyseal.api

class UrlMappings {

    static mappings = {
        post "/changeLog"(controller:"changeLog", action:"list")
        post "/member"(controller:"member", action:"getMember")
    }

}
