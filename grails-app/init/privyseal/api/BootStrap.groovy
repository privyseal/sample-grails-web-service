package privyseal.api

class BootStrap {

    def init = { servletContext ->

        println "Creating dummy data";
        println "===================";

        addRecord(new ChangeLog(changeType:"update", memberNumber:"1818272", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"insert", memberNumber:"2827763", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"insert", memberNumber:"9191726", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"update", memberNumber:"1818272", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"update", memberNumber:"2827763", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"update", memberNumber:"1818272", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"update", memberNumber:"2827763", changeDate: new Date()));
        addRecord(new ChangeLog(changeType:"update", memberNumber:"9191726", changeDate: new Date()));

        addRecord(new Member(name:"John Smith", memberNumber:"1818272", mobileNumber: "+27829991818", emailAddress: "john@email.com"));
        addRecord(new Member(name:"Frank Mabatho", memberNumber:"2827763", mobileNumber: "+27992827711", emailAddress: "frank@email.com"));
        addRecord(new Member(name:"Solly Sibanda", memberNumber:"9191726", mobileNumber: "+2711991111", emailAddress: "solly@email.com"));

    }
    def destroy = {
    }

    def addRecord(record) {
        if (!record.save()) {
            println "Error ${record.errors.allErrors}"
        }
        println record
    }
    
}
